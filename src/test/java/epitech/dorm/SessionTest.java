/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epitech.dorm;

import epitech.orm.configuration.Configuration;
import epitech.orm.exception.SQLConnectionException;
import epitech.orm.exception.TableDefinitionNotFound;
import epitech.orm.session.Session;
import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;
import junit.framework.Assert;
import junit.framework.TestCase;

/**
 *
 * @author jalal
 */
public class SessionTest extends TestCase
{
    public void testSession()
    {
        Session session = null;
        
        try 
        {
            try 
            {
                session = new Session(Configuration.createFromFile("config.xml"));
            } 
            
            catch (SQLConnectionException ex) 
            {
                Assert.fail("Problem when trying to connect to the database : " + ex.getMessage());
                Logger.getLogger(SessionTest.class.getName()).log(Level.SEVERE, ex.getMessage());
            }
        } 
        catch (FileNotFoundException | TableDefinitionNotFound ex) 
        {
            Assert.fail("Problem when trying to parse configuration : " + ex.getMessage());
            Logger.getLogger(SessionTest.class.getName()).log(Level.SEVERE, ex.getMessage());
        }
        finally
        {
            if (session != null)
                session.close();
        }
    }
 
}
