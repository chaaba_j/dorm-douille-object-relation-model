/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epitech.dorm;

import epitech.dorm.object.Category;
import epitech.dorm.object.Test;
import epitech.orm.configuration.Configuration;
import epitech.orm.exception.TableDefinitionNotFound;
import epitech.orm.session.Session;
import java.sql.SQLException;
import junit.framework.TestCase;
import org.junit.Assert;

/**
 *
 * @author jalal
 */
public class UpdateQueryTest extends TestCase {

    private Session session = null;

    @Override
    public void setUp() throws Exception {
        super.setUp();

        session = new Session(Configuration.createFromFile("config.xml"));
    }

    @Override
    public void tearDown() throws Exception {
        super.tearDown();

        if (session != null) {
            session.close();
        }
    }

    public void testUpdate() throws TableDefinitionNotFound, SQLException {

        Test test = new Test();

        test.setId(1);
        test.setData("marcha");
        test = session.save(test);
        Assert.assertEquals(test.getData(), "marcha");

    }

    public void testUpdateOneToMany() throws TableDefinitionNotFound, SQLException {

        Category category = (Category) session.select(Category.class).first().execute().unique();

        category.setTitle("test title");
        category = session.save(category);
        Assert.assertEquals("test title", category.getTitle());
    }
}
