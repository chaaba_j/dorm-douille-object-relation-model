/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epitech.dorm;

import epitech.dorm.object.Test;
import epitech.orm.configuration.Configuration;
import epitech.orm.exception.TableDefinitionNotFound;
import epitech.orm.session.Session;
import java.sql.SQLException;
import java.util.List;
import junit.framework.TestCase;

/**
 *
 * @author jalal
 */
public class DeleteQueryTest extends TestCase {

    public Session session = null;

    @Override
    public void setUp() throws Exception {
        super.setUp();

        session = new Session(Configuration.createFromFile("config.xml"));
    }
    
    @Override
    public void tearDown() throws Exception
    {
        super.tearDown();
        
        if (session != null)
            session.close();
    }

    public void testDelete() throws TableDefinitionNotFound, SQLException {

        List<Test> result;

        result = (List<Test>) session.select(Test.class).limit(0, 5).execute().list();

        for (Test test : result) {
            session.delete(test);
        }

    }
}
