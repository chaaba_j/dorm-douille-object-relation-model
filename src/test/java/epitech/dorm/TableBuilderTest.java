/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epitech.dorm;

import epitech.orm.query.builder.ISQLTableBuilder;
import epitech.orm.query.builder.SQLTableBuilder;
import epitech.orm.sql.Column;
import epitech.orm.sql.ColumnMetaData;
import junit.framework.Assert;
import junit.framework.TestCase;

/**
 *
 * @author jalal
 */
public class TableBuilderTest extends TestCase {

    public void testBuildTable() throws Exception 
    {
        Column column = new Column();
        ColumnMetaData metaData = new ColumnMetaData();
        ISQLTableBuilder builder = new SQLTableBuilder();
        String query;

        metaData.setAutoIncrement(true);
        metaData.setDefaultAttribute(ColumnMetaData.DefaultValueAttribute.None);
        metaData.setIndexAttribute(ColumnMetaData.IndexAttribute.PrimaryKey);
        metaData.setType(ColumnMetaData.ColumnType.Integer);
        column.setMetaData(metaData);
        column.setColumnName("id");
        column.setObjectPropertyName("id");
        column.setSqlGeneratorFunction("UID");
        builder.create("test");
        builder.addField(column);
        query = builder.build();
        Assert.assertEquals("CREATE TABLE IF NOT EXISTS test(id INTEGER NOT NULL AUTO_INCREMENT ,PRIMARY KEY (id))ENGINE=InnoDB DEFAULT CHARSET=utf8",  query);
    }
}
