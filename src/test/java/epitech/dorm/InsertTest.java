/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epitech.dorm;

import epitech.orm.configuration.Configuration;
import epitech.orm.configuration.TableDefinition;
import epitech.orm.exception.IncompleteSQLRequestException;
import epitech.orm.exception.TableDefinitionNotFound;
import epitech.orm.query.builder.SQLInsertBuilder;
import epitech.orm.sql.Column;
import java.io.FileNotFoundException;
import junit.framework.Assert;
import junit.framework.TestCase;

/**
 *
 * @author jalal
 */
public class InsertTest extends TestCase {

    private Configuration config = null;

    @Override
    public void setUp() throws FileNotFoundException, TableDefinitionNotFound, Exception {
        super.setUp();

        config = Configuration.createFromFile("config.xml");
    }

    public void testInsert() throws IncompleteSQLRequestException {

        TableDefinition definition = config.getTableDefinition().get(0);
        SQLInsertBuilder query = new SQLInsertBuilder(definition);
        Column column = definition.getColumns().get(0);

        Assert.assertEquals("INSERT INTO toto (id) VALUES (\"toto\") ;", query.insert().forColumn(column.getColumnName(), "toto").build());

    }
}
