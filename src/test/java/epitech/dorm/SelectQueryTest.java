/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epitech.dorm;

import epitech.dorm.object.Ben;
import epitech.dorm.object.Category;
import epitech.dorm.object.News;
import epitech.dorm.object.Person;
import epitech.dorm.object.Test;
import epitech.orm.configuration.Configuration;
import epitech.orm.exception.TableDefinitionNotFound;
import epitech.orm.query.Restriction;
import epitech.orm.session.Session;
import java.sql.SQLException;
import java.util.List;

import junit.framework.TestCase;
import org.junit.Assert;



/**
 *
 * @author jalal
 */
public class SelectQueryTest extends TestCase {

    private Session session = null;

    /**
     *
     * @throws java.lang.Exception
     */
    @Override
    public void setUp() throws Exception {
        super.setUp();
        session = new Session(Configuration.createFromFile("config.xml"));
    }
    
    /**
     *
     * @throws Exception
     */
    @Override
    public void tearDown() throws Exception
    {
        super.tearDown();
        
        if (session != null)
            session.close();
    }

    public void testSelectQuery() throws TableDefinitionNotFound, SQLException {
        List<Test> result;

        result = (List<Test>) session.select(Test.class).execute().list();

        for (Test test : result) {
            System.out.println(test.getData());
        }
    }

    public void testSelectQueryWithManyClause() throws TableDefinitionNotFound, SQLException {

        List<Test> result;

        result = (List<Test>) session.select(Test.class)
                .where(Restriction.greaterOrEqual("id", 1))
                .and(Restriction.lowerOrEqual("id", 3))
                .execute().list();

        for (Test test : result) {
            System.out.println(test.getData());
            System.out.println(test.getId());
        }

    }

    public void testSelectWithLimit() throws TableDefinitionNotFound, SQLException {
        List<Test> result;

        result = (List<Test>) session.select(Test.class).limit(0, 5).execute().list();

        for (Test test : result) {
            System.out.println(test.getData());
            System.out.println(test.getId());
        }

    }
    
    public void testSelectWithInvalidClass() throws SQLException
    {
        try 
        {
            session.select(String.class).execute().list();
        } 
        catch (TableDefinitionNotFound ex) 
        {
            Assert.assertEquals(ex.getMessage(), "No such table for class : java.lang.String");
        }
    }
    
    public void testSelectOneToMany() throws TableDefinitionNotFound, SQLException {
        List<Category> result;

        result = (List<Category>) session.select(Category.class).execute().list();

        System.out.println("Print category result");
        for (Category category : result) {
            System.out.println(category.getTitle());
            if (category.getNewsList() != null) {
                for (News news : category.getNewsList()) {
                    System.out.println(news.getTitle());
                }
            }
        }

    }

    public void testSelectOneToOne() throws TableDefinitionNotFound, SQLException {
        List<Ben> result;

        result = (List<Ben>) session.select(Ben.class).execute().list();

        System.out.println("Print category result");
        for (Ben ben : result) {
            if (ben.getPerez() != null) 
            {
                Assert.assertEquals(ben.getPerez().getSkuff(), 1000000);
            }
        }
    }
    
    public void testSelectManyToOne() throws TableDefinitionNotFound, SQLException
    {
        List<Person>    results;
        
        results = (List<Person>) session.select(Person.class).execute().list();
        
        for (Person person : results)
        {
            System.out.println(person.getFirstname());
            Assert.assertNotNull(person.getAddress());
            Assert.assertNotNull(person.getAddress().getCity());
        }
    }
}
