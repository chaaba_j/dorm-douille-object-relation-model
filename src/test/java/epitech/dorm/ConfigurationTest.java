/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epitech.dorm;

import epitech.orm.configuration.Configuration;
import epitech.orm.configuration.TableDefinition;
import epitech.orm.exception.TableDefinitionNotFound;
import epitech.orm.sql.ColumnMetaData;
import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;
import junit.framework.Assert;
import junit.framework.TestCase;

/**
 *
 * @author jalal
 */
public class ConfigurationTest extends TestCase {
    

      
    public void testInitConfiguration()
    {
        try 
        {
            Configuration   config = Configuration.createFromFile("src/test/java/epitech/dorm/test/config/config.xml");
            TableDefinition definition = config.getTableDefinition().get(0);
            
            assertEquals(definition.getTableName(), "toto");
            assertEquals(definition.getColumns().get(0).getMetaData().getIndexAttribute(), ColumnMetaData.IndexAttribute.PrimaryKey);
        } 
        catch (FileNotFoundException ex) 
        {
            Logger.getLogger(ConfigurationTest.class.getName()).log(Level.SEVERE, ex.getMessage());
            Assert.fail("file doesn't exist");
        } 
        catch (TableDefinitionNotFound ex) 
        {
            Logger.getLogger(ConfigurationTest.class.getName()).log(Level.SEVERE, ex.getMessage());
            Assert.fail("Table definition not found : " + ex.getMessage());
        }
    }
}
