/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epitech.dorm;

import epitech.dorm.object.Address;
import epitech.dorm.object.Ben;
import epitech.dorm.object.Category;
import epitech.dorm.object.News;
import epitech.dorm.object.Perez;
import epitech.dorm.object.Person;
import epitech.dorm.object.Test;
import epitech.orm.configuration.Configuration;
import epitech.orm.exception.TableDefinitionNotFound;
import epitech.orm.query.Restriction;
import epitech.orm.session.Session;
import epitech.orm.session.Transaction;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import junit.framework.TestCase;
import org.junit.Assert;

/**
 *
 * @author jalal
 */
public class InsertQueryTest extends TestCase {

    private Session session = null;

    @Override
    public void setUp() throws Exception {
        super.setUp();

        session = new Session(Configuration.createFromFile("config.xml"));
    }

    @Override
    public void tearDown() throws Exception {
        super.tearDown();

        if (session != null) {
            session.close();
        }
    }

    public void testInsertQuery() throws TableDefinitionNotFound, SQLException {

        Test test = new Test();

        test.setData("bonjour a tous");
        session.save(test);

    }

    public void testOneToManyInsert() throws TableDefinitionNotFound, SQLException {

        List<News> newsList = new ArrayList<>();
        Category category = new Category();
        News news = new News();
        News news2 = new News();

        news.setContent("BLA BLA BLA BLA BLA BLA BLA BLA BLA BLA");
        news.setTitle("BLABLA");
        news2.setTitle("TOTOOTOTOTOTOT");
        news2.setContent("BLBLBDBDSBS");
        category.setDescription("BLA BLA BLA BLA");
        category.setTitle("Bonjour a tous");
        newsList.add(news);
        newsList.add(news2);
        category.setNewsList(newsList);
        session.save(category);
    }

    public void testOneToOneInsert() throws SQLException, TableDefinitionNotFound {

        Ben ben = new Ben();
        Perez perez = new Perez();

        perez.setData("Grosse douille");
        perez.setSkuff(1000000);
        ben.setData("Koala");
        ben.setPerez(perez);
        session.save(ben);
    }
    
    public void testManyToOneInsert() throws SQLException
    {
        Person  person = new Person();
        Address address = (Address) session.select(Address.class).first().execute().unique();
        
        person.setAddress(address);
        person.setFirstname("toto");
        person.setLastname("marcha");
        person = session.save(person);
        Assert.assertEquals("toto", person.getFirstname());
        Assert.assertEquals("marcha", person.getLastname());
    }
    
    public void testSelectWithIn() throws SQLException
    {
        Integer ids[] =
        {
            5, 4, 3, 2, 1
        };
        
        List<Perez> results = (List<Perez>) session.select(Perez.class).where(Restriction.in("id", ids)).execute().list();
        
        Assert.assertTrue(results.size() > 0);
    } 
}
