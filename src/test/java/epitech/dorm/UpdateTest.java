/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epitech.dorm;

import epitech.orm.configuration.Configuration;
import epitech.orm.exception.TableDefinitionNotFound;
import epitech.orm.query.builder.SQLSaveBuilder;
import java.io.FileNotFoundException;
import junit.framework.Assert;
import junit.framework.TestCase;

/**
 *
 * @author jalal
 */
public class UpdateTest extends TestCase {

    private Configuration config = null;

    @Override
    public void setUp() throws FileNotFoundException, TableDefinitionNotFound {
        config = Configuration.createFromFile("src/test/java/epitech/dorm/test/config/config.xml");
    }

    public void testUpdate() {
        SQLSaveBuilder query = new SQLSaveBuilder(config.getTableDefinition().get(0));

        Assert.assertEquals("UPDATE toto SET hello=\"bonjour\", blabla=5", query.update("hello", "bonjour").set("blabla", 5).build());

    }
}
