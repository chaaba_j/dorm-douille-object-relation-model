/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epitech.dorm;

import epitech.orm.configuration.Configuration;
import epitech.orm.exception.TableDefinitionNotFound;
import epitech.orm.query.Restriction;
import epitech.orm.query.builder.SQLDeleteBuilder;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import junit.framework.Assert;
import junit.framework.TestCase;

/**
 *
 * @author jalal
 */
public class DeleteTest extends TestCase {

    private Configuration config = null;

    @Override
    public void setUp() throws Exception {
        super.setUp();

        config = Configuration.createFromFile("config.xml");
    }

    public void testDelete() {

        SQLDeleteBuilder query = new SQLDeleteBuilder(config.getTableDefinition().get(0));
        Restriction restriction = Restriction.equal("skuff", 10);
        List<Restriction> restrictions = new ArrayList<>();

        restriction.setRestrictionTag(Restriction.RestrictionTag.And);
        restrictions.add(restriction);
        restrictions.add(Restriction.lower("skuff", 20));
        Assert.assertEquals("DELETE FROM toto WHERE (skuff = 10 AND skuff < 20 );", query.delete().restrict(restrictions).build());
    }
}
