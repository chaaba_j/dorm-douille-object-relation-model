/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epitech.dorm;

import epitech.orm.configuration.Configuration;
import epitech.orm.query.Restriction;
import epitech.orm.query.builder.ISQLSelectBuilder;
import epitech.orm.query.builder.SQLSelectBuilder;

import java.util.ArrayList;
import java.util.List;
import junit.framework.Assert;
import junit.framework.TestCase;

/**
 *
 * @author jalal
 */
public class SelectTest extends TestCase {

    private Configuration config = null;

    @Override
    public void setUp() throws Exception {
        super.setUp();
        config = Configuration.createFromFile("config.xml");
    }

    public void testSelect() {

        SQLSelectBuilder query = new SQLSelectBuilder(config.getTableDefinition().get(0));
        Restriction restriction = Restriction.equal("hello", "coucou");
        List<Restriction> restrictions = new ArrayList<>();

        restriction.setRestrictionTag(Restriction.RestrictionTag.And);
        restrictions.add(restriction);
        restrictions.add(Restriction.equal("bonjour", 5).and(Restriction.equal("blabla", 10).or(Restriction.like("toto", "titi"))));
        query.select().restrict(restrictions).limit(0, 10).sort(ISQLSelectBuilder.Order.ASC, "bonjour");
        Assert.assertEquals("SELECT * FROM toto WHERE (hello = \"coucou\" AND (bonjour = 5 AND ((blabla = 10 OR (toto LIKE \"titi\" ))))) ORDER BY bonjour ASC  LIMIT 0, 10", query.build());
    }

    public void testSelectIn() {
        String[] values = {"toto", "marcha"};

        SQLSelectBuilder query = new SQLSelectBuilder(config.getTableDefinition().get(0));
        Restriction restriction = Restriction.in("skuff", values);
        List<Restriction> restrictions = new ArrayList<>();

        restriction.setRestrictionTag(Restriction.RestrictionTag.And);
        restrictions.add(restriction);
        restrictions.add(Restriction.equal("bonjour", 5).and(Restriction.equal("blabla", 10).or(Restriction.like("toto", "titi"))));
        query.select().restrict(restrictions).limit(0, 10).sort(ISQLSelectBuilder.Order.ASC, "bonjour");
        Assert.assertEquals("SELECT * FROM toto WHERE (skuff IN (\"toto\",\"marcha\") AND (bonjour = 5 AND ((blabla = 10 OR (toto LIKE \"titi\" ))))) ORDER BY bonjour ASC  LIMIT 0, 10", query.build());

    }
}
