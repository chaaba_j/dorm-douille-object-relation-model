/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epitech.dorm.object;

/**
 *
 * @author jalal
 */
public class Test 
{
    private int     id;
    private String  data;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the data
     */
    public String getData() {
        return data;
    }

    /**
     * @param data the data to set
     */
    public void setData(String data) {
        this.data = data;
    }   
}
