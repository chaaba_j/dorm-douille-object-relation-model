/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package epitech.dorm.object;

/**
 *
 * @author chaabane
 */
public class Perez 
{

    private int     id;
    private String  data;
    private int     skuff;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the data
     */
    public String getData() {
        return data;
    }

    /**
     * @param data the data to set
     */
    public void setData(String data) {
        this.data = data;
    }

    /**
     * @return the skuff
     */
    public int getSkuff() {
        return skuff;
    }

    /**
     * @param skuff the skuff to set
     */
    public void setSkuff(int skuff) {
        this.skuff = skuff;
    }
    
    
    
    
}
