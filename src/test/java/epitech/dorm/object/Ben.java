/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package epitech.dorm.object;

/**
 *
 * @author chaabane
 */
public class Ben 
{
    private int     id;
    private String  data;
    private Perez   perez;

    
    
    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the data
     */
    public String getData() {
        return data;
    }

    /**
     * @param data the data to set
     */
    public void setData(String data) {
        this.data = data;
    }

    /**
     * @return the perez
     */
    public Perez getPerez() {
        return perez;
    }

    /**
     * @param perez the perez to set
     */
    public void setPerez(Perez perez) {
        this.perez = perez;
    }
    
    
}
