/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epitech.orm.utils;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import org.apache.commons.lang.WordUtils;

/**
 *
 * @author jalal
 */
public class ReflectUtils 
{
    public static <T> T    getAttribute(T obj, String name) throws NoSuchFieldException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException
    {
        Method  method = ReflectUtils.getGetter(obj.getClass(), name);
        
        return (T)method.invoke(obj);
    }
    
    public static <T, U> T  makeInstance(Class<T> clazz, U ... params) throws NoSuchMethodException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException
    {
        Constructor constructor = clazz.getConstructor();
        
        return (T)constructor.newInstance();
    }
    
    public static <T, U> void setAttribute(T obj, String name, U value) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException
    {
        Method  method = ReflectUtils.getSetter(obj.getClass(), name, value.getClass());
        
        method.invoke(obj, value);
    }
    
    private static <T> Method  getGetter(Class<T> clazz, String name) throws NoSuchMethodException
    {
        return searchMethodByName(clazz, "get" + WordUtils.capitalize(name));
    }
    
    private static <T> Method   searchMethodByName(Class<T> clazz, String name) throws NoSuchMethodException
    {
        for (Method method : clazz.getMethods())
        {
            if (method.getName().compareTo(name) == 0)
            {
                return method;
            }
        }
        throw new NoSuchMethodException("Cannot find method : " + name);
    }
    
    private static <T, U> Method  getSetter(Class<T> clazz, String name, Class<U> valueClass) throws NoSuchMethodException
    {
        return searchMethodByName(clazz, "set" + WordUtils.capitalize(name));
    }
}
