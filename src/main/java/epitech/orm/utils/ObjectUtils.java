/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epitech.orm.utils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

/**
 *
 * @author jalal
 */
public class ObjectUtils 
{
    public static <T> void   merge(T src, T dest) throws NoSuchFieldException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException
    {
        Object  value;
        Object  destValue;
        Field   fields[] = src.getClass().getDeclaredFields();
        
        for (Field field : fields)
        {
            value = ReflectUtils.getAttribute(src, field.getName());
            destValue = ReflectUtils.getAttribute(dest, field.getName());
            if (value != null)
            {
                ReflectUtils.setAttribute(dest, field.getName(), value);
            }
        }
    }
}
