/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epitech.orm.exception;

/**
 *
 * @author jalal
 */
public class DORMException extends RuntimeException
{
    protected DORMException()
    {

    }

    public DORMException(String message)
    {
	super(message);
    }
}
