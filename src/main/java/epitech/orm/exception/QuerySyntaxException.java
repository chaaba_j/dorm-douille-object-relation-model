/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epitech.orm.exception;

/**
 *
 * @author jalal
 */
public class QuerySyntaxException extends DORMException
{
        protected QuerySyntaxException()
	{

	}

	public QuerySyntaxException(String message)
	{
		super(message);
	}
}
