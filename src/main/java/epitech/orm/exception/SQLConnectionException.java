/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epitech.orm.exception;

import java.sql.SQLException;

/**
 *
 * @author jalal
 */
public class SQLConnectionException extends SQLException
{
    protected SQLConnectionException()
    {

    }

     public SQLConnectionException(String message)
     {
	super(message);
     }
}
