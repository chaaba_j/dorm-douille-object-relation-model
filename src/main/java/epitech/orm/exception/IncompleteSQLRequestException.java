/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epitech.orm.exception;

/**
 *
 * @author jalal
 */
public class IncompleteSQLRequestException extends DORMException
{
    	protected IncompleteSQLRequestException()
	{

	}

	public IncompleteSQLRequestException(String message)
	{
		super(message);
	}
}
