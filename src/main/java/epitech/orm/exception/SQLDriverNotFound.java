/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epitech.orm.exception;

/**
 *
 * @author jalal
 */
public class SQLDriverNotFound extends RuntimeException
{
     protected SQLDriverNotFound()
     {

     }

     public SQLDriverNotFound(String message)
     {
	super(message);
     }
}
