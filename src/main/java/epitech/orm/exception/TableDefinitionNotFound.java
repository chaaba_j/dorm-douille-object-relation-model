/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epitech.orm.exception;

/**
 *
 * @author jalal
 */
public class TableDefinitionNotFound extends DORMException
{
	protected TableDefinitionNotFound()
	{

	}

	public TableDefinitionNotFound(String message)
	{
		super(message);
	}
}
