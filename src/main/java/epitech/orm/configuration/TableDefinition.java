/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epitech.orm.configuration;

import epitech.orm.configuration.parser.XMLTableDefinitionParser;
import epitech.orm.sql.Column;
import epitech.orm.sql.ColumnMetaData;
import java.io.FileNotFoundException;
import java.util.List;

/**
 *
 * @author jalal
 */
public class TableDefinition 
{
    
    private List<Column>    _columns;
    private Class           _objectClass;
    private String          _tableName;
    
    public static TableDefinition create(String file) throws FileNotFoundException
    {
        
        XMLTableDefinitionParser reader = new XMLTableDefinitionParser(file);
        
        return reader.parse();
    }
    
    public Column   getPrimaryKey()
    {
        for (Column column : this._columns)
        {
            if (column.getMetaData().getIndexAttribute() == ColumnMetaData.IndexAttribute.PrimaryKey)
                return column;
        }
        return null;
    }
    
    
    public TableDefinition()
    {
    }
    
    public Class    getObjectClass()
    {
        return this._objectClass;
    }
    
    public void     setObjectClassForName(String name) throws ClassNotFoundException
    {
        this.setObjectClass(Class.forName(name));
    }
    
    /**
     * @return the _columns
     */
    public List<Column> getColumns() {
        return _columns;
    }

    /**
     * @param columns the _columns to set
     */
    public void setColumns(List<Column> columns) {
        this._columns = columns;
    }   

    /**
     * @param objectClass the _objectClass to set
     */
    public void setObjectClass(Class objectClass) {
        this._objectClass = objectClass;
    }

    /**
     * @return the _tableName
     */
    public String getTableName() {
        return _tableName;
    }

    /**
     * @param tableName the _tableName to set
     */
    public void setTableName(String tableName) {
        this._tableName = tableName;
    }
}
