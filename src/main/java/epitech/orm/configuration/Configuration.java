/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epitech.orm.configuration;

import epitech.orm.configuration.parser.SQLRelationLinker;
import epitech.orm.configuration.parser.XMLConfigurationParser;
import epitech.orm.exception.TableDefinitionNotFound;
import java.io.FileNotFoundException;
import java.util.List;

/**
 *
 * @author jalal
 */
public class Configuration 
{
    private String                  _username;
    private String                  _password;
    private String                  _databaseUrl;
    private String                  _driver;
    private List<TableDefinition>   _tablesDefinitions;
    
    public static final String      sqlEngine = "InnoDB";
    public static final String      charset = "utf8";
    
    
    public Configuration()
    {
        
    }
    
    public static Configuration createFromFile(String file) throws FileNotFoundException, TableDefinitionNotFound
    {
       Configuration            config = null;
       SQLRelationLinker        linker = null;
       XMLConfigurationParser   reader = new XMLConfigurationParser(file);
       
       config = reader.parse();
       config._tablesDefinitions = reader.parseTableDefintion();
       linker = new SQLRelationLinker(config._tablesDefinitions);
       linker.link();
       return config;
    }
    
    public List<TableDefinition>    getTableDefinition()
    {
        return this._tablesDefinitions;
    }
    /**
     * @return the _username
     */
    public String getUsername() {
        return _username;
    }

    /**
     * @param username the _username to set
     */
    public void setUsername(String username) {
        this._username = username;
    }

    /**
     * @return the _password
     */
    public String getPassword() {
        return _password;
    }

    /**
     * @param password the _password to set
     */
    public void setPassword(String password) {
        this._password = password;
    }

    /**
     * @return the _driver
     */
    public String getDriver() {
        return _driver;
    }

    /**
     * @param driver the _driver to set
     */
    public void setDriver(String driver) {
        this._driver = driver;
    }

    /**
     * @return the _databaseUrl
     */
    public String getDatabaseUrl() {
        return _databaseUrl;
    }

    /**
     * @param databaseUrl the _databaseUrl to set
     */
    public void setDatabaseUrl(String databaseUrl) {
        this._databaseUrl = databaseUrl;
    }
}
