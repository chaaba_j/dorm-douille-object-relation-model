/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epitech.orm.configuration.parser;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.jdom.Document;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

/**
 *
 * @author jalal
 */
public class BaseXMLParser
{
    protected File      _file;
    
    public BaseXMLParser(String file) throws FileNotFoundException
    {
        this._file = new File(file);
        
        if (!this._file.exists())
            throw new FileNotFoundException("Cannot find file : " + this._file.getAbsolutePath());
    }

    public Document loadDocument() throws JDOMException, IOException 
    {
        SAXBuilder builder = null;
        Document document = null;
        
        builder = new SAXBuilder();
        document = builder.build(this._file);
        return document;
    }
}
