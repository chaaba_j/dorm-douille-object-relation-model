/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epitech.orm.configuration.parser;



/**
 *
 * @author jalal
 * @param <T>
 */
public interface IParser<T>
{
    public T  parse() throws Exception;
}
