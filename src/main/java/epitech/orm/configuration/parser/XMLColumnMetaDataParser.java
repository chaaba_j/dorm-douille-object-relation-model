/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epitech.orm.configuration.parser;

import epitech.orm.sql.ColumnMetaData;
import java.util.HashMap;
import java.util.Map;
import org.jdom.Element;

/**
 *
 * @author jalal
 */
public class XMLColumnMetaDataParser implements IParser<ColumnMetaData>
{
    private Element                         _columnElement = null;
    private Element                         _propertyElement = null;
    private ColumnMetaData.IndexAttribute   _indexAttr = null;
    
    private static final Map<String, ColumnMetaData.ColumnType>   _types = new HashMap();
    static 
    {
        _types.put("string", ColumnMetaData.ColumnType.String);
        _types.put("datetime", ColumnMetaData.ColumnType.Date);
        _types.put("integer", ColumnMetaData.ColumnType.Integer);
        _types.put("float", ColumnMetaData.ColumnType.Float);
        _types.put("char", ColumnMetaData.ColumnType.Char);
    };
    
    
    public XMLColumnMetaDataParser(Element propertyElement, Element columnElement, ColumnMetaData.IndexAttribute attribute)
    {
        this._columnElement = columnElement;
        this._propertyElement = propertyElement;
        this._indexAttr = attribute;
    }
    
    @Override
    public ColumnMetaData  parse()
    {
        ColumnMetaData  metaData = new ColumnMetaData();
        
        metaData.setIndexAttribute(this._indexAttr);
        metaData.setType(_types.get(this._propertyElement.getAttributeValue("type")));
        
        if (this._columnElement.getAttributeValue("not-null") != null)
        {
            metaData.setIsNullable(Boolean.parseBoolean(this._columnElement.getAttributeValue("not-null")));
        }
        if (this._columnElement.getAttributeValue("length") != null)
        {
            metaData.setLength(Integer.parseInt(this._columnElement.getAttributeValue("length")));
        }
        if (this._columnElement.getAttributeValue("auto-increment") != null)
        {
            metaData.setAutoIncrement(Boolean.parseBoolean(this._columnElement.getAttributeValue("auto-increment")));
        }
        return metaData;
    }
}
