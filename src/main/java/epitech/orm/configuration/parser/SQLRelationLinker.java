/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epitech.orm.configuration.parser;

import epitech.orm.configuration.TableDefinition;
import epitech.orm.exception.TableDefinitionNotFound;
import epitech.orm.sql.Column;
import epitech.orm.sql.SQLRelation;
import java.util.List;

/**
 *
 * @author jalal
 */
public class SQLRelationLinker 
{
    private List<TableDefinition>   _definitions = null;
    
    public SQLRelationLinker(List<TableDefinition> definitions)
    {
        this._definitions = definitions;
    }
    
    public final void link() throws TableDefinitionNotFound
    {
        SQLRelation     relation;
        
        for (TableDefinition definition : this._definitions)
        {
            for (Column column : definition.getColumns())
            {
                if (column.getRelation() != null)
                {
                    this.linkTable(column.getRelation());
                }
            }
        }
    }
    
    private void linkTable(SQLRelation relation) throws TableDefinitionNotFound
    {
        for (TableDefinition definition : this._definitions)
        {
            if (definition.getObjectClass().getName().compareTo(relation.getTargetSymbol()) == 0)
            {
               relation.setTableDefinition(definition);
               return ; 
            }
        }
        throw new TableDefinitionNotFound("Cannot found a table definition for relation with symbol : " + relation.getTargetSymbol());
    }
}
