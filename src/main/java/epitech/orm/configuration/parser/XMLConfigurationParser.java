/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epitech.orm.configuration.parser;

import epitech.orm.configuration.Configuration;
import epitech.orm.configuration.TableDefinition;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;

import org.jdom.xpath.XPath;

/**
 *
 * @author jalal
 */
public class XMLConfigurationParser extends BaseXMLParser implements IConfigurationParser, IParser<Configuration>
{
    private static final String                 _databaseUrlPropertyName = "douille.connection.url";
    private static final String                 _driverPropertyName = "douille.connection.driver_class";
    private static final String                 _userNamePropertyName = "douille.connection.username";
    private static final String                 _passwordPropertyName = "douille.connection.password";
    private List<String>                        _tableDefinitionFiles = new ArrayList<>();

    
    public XMLConfigurationParser(String filename) throws FileNotFoundException
    {
        super(filename);
    }
    
    @Override
    public Configuration    parse()
    {
        Configuration   config = null;
        Document        document = null;
        Element         rootElement = null;
        
        try 
        {
            config = new Configuration();
            document = this.loadDocument();
            rootElement = document.getRootElement();
            this.parseConfiguration(config, rootElement);
            return config;
        }
        catch (JDOMException | IOException ex) 
        {
            Logger.getLogger(XMLConfigurationParser.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    @Override
    public List<TableDefinition>    parseTableDefintion() throws FileNotFoundException
    {
        List<TableDefinition>        defintions = new ArrayList();
        
        for (String tableDefintionFile : this._tableDefinitionFiles)
        {
            defintions.add(TableDefinition.create(tableDefintionFile));
        }
        return defintions;
    }
    
    private String  getStringFromXPath(XPath xpath, Element element) throws JDOMException
    {
        Object obj = xpath.selectSingleNode(element);
        
        return xpath.valueOf(obj);
    }
    
    private void    parseConfiguration(Configuration config, Element element) throws JDOMException
    {   
        XPath            mappingPath = XPath.newInstance("//mapping");
        List<Element>    mappingList = null;
        
        config.setDriver(this.getStringFromXPath(XPath.newInstance("//property[@name='" + XMLConfigurationParser._driverPropertyName + "']"), element));
        config.setDatabaseUrl(this.getStringFromXPath(XPath.newInstance("//property[@name='" + XMLConfigurationParser._databaseUrlPropertyName + "']"), element));
        config.setUsername(this.getStringFromXPath(XPath.newInstance("//property[@name='" + XMLConfigurationParser._userNamePropertyName + "']"), element));
        config.setPassword(this.getStringFromXPath(XPath.newInstance("//property[@name='" + XMLConfigurationParser._passwordPropertyName + "']"), element));
        
        mappingList = mappingPath.selectNodes(element);
        for (Element elem : mappingList)
        {
            this._tableDefinitionFiles.add(elem.getAttributeValue("resource"));
        }
    }    
}
