/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epitech.orm.configuration.parser;

import epitech.orm.sql.Column;
import epitech.orm.sql.ColumnMetaData;
import epitech.orm.sql.SQLRelation;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.xpath.XPath;

/**
 *
 * @author jalal
 */
public class XMLSQLRelationParser implements IParser<Column>
{

    private Element _relationElement;
    
    public XMLSQLRelationParser(Element relationElement)
    {
        this._relationElement = relationElement;
    }

    @Override
    public Column parse() throws JDOMException, ClassNotFoundException 
    {
        XMLColumnMetaDataParser metaDataParser;
        SQLRelation relation = new SQLRelation();
        Element columnElement;
        Column  column = new Column();
        XPath   xpath = XPath.newInstance("./column");
        
        column.setObjectPropertyName(this._relationElement.getAttributeValue("name"));
        
        relation.setTargetSymbol(this._relationElement.getAttributeValue("class"));
        relation.setPropertyName(this._relationElement.getAttributeValue("property_name"));
        columnElement = (Element) xpath.selectSingleNode(this._relationElement);
        column.setColumnName(columnElement.getAttributeValue("name"));
        metaDataParser = new XMLColumnMetaDataParser(columnElement, columnElement, ColumnMetaData.IndexAttribute.ForeignKey);
        column.setRelation(relation);
        column.setMetaData(metaDataParser.parse());
        return column;
    }
}
