/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epitech.orm.configuration.parser;

import epitech.orm.configuration.TableDefinition;
import java.io.FileNotFoundException;
import java.util.List;

/**
 *
 * @author jalal
 */
public interface IConfigurationParser
{
    public List<TableDefinition>    parseTableDefintion() throws FileNotFoundException;
}
