/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epitech.orm.configuration.parser;

import epitech.orm.configuration.TableDefinition;
import epitech.orm.sql.Column;
import epitech.orm.sql.ColumnMetaData;
import epitech.orm.sql.SQLRelation;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.xpath.XPath;


/**
 *
 * @author jalal
 */
public class XMLTableDefinitionParser extends BaseXMLParser implements IParser<TableDefinition>
{    
    /**
     *
     * @param filename
     */
    

    
    public XMLTableDefinitionParser(String filename) throws FileNotFoundException
    {
        super(filename);
    }

    @Override
    public TableDefinition parse() 
    {
        List<Column>    columns = new ArrayList<>();
        TableDefinition definition = new TableDefinition();
        Element         rootElement = null;
        Document        document = null;
        
        try 
        {
            document = this.loadDocument();
            rootElement = document.getRootElement();
            this.parseId(columns, rootElement);
            this.parseColumns(columns, rootElement);
            this.parseClass(definition, rootElement);
            this.parseRelation(columns, rootElement);
            definition.setColumns(columns);
            return definition;
        }
        catch (JDOMException | IOException | ClassNotFoundException ex) 
        {
            Logger.getLogger(XMLTableDefinitionParser.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    private void    parseRelation(List<Column> columns, Element rootElement) throws JDOMException, ClassNotFoundException
    {
        XMLSQLRelationParser    relationParser;
        Column                  column;
        List<Element>           elements;
        XPath                   xpathOneToMany = XPath.newInstance("//one-to-many");
        XPath                   xpathOneToOne = XPath.newInstance("//one-to-one");
        XPath                   xpathManyToOne = XPath.newInstance("//many-to-one");
        
        elements = xpathOneToMany.selectNodes(rootElement);
        for (Element element : elements)
        {
            relationParser = new XMLSQLRelationParser(element);
            column = relationParser.parse();
            column.getRelation().setRelationType(SQLRelation.RelationType.OneToMany);
            columns.add(column);
        }
        
        elements = xpathOneToOne.selectNodes(rootElement);
        
        for (Element element : elements)
        {
            relationParser = new XMLSQLRelationParser(element);
            column = relationParser.parse();
            column.getRelation().setRelationType(SQLRelation.RelationType.OneToOne);
            columns.add(column);
        }
        
        elements = xpathManyToOne.selectNodes(rootElement);
        for (Element element : elements)
        {
            relationParser = new XMLSQLRelationParser(element);
            column = relationParser.parse();
            column.getRelation().setRelationType(SQLRelation.RelationType.ManyToOne);
            columns.add(column);
        }
    }
    
    private void    parseId(List<Column> columns, Element rootElement) throws JDOMException
    {
        Column  column = new Column();
        XPath   xpath = XPath.newInstance("//id");
        Element element;
        Element subElement;
        
        element = (Element) xpath.selectSingleNode(rootElement);

        column.setObjectPropertyName(element.getAttributeValue("name"));
        
        xpath = XPath.newInstance("./column");
        
        subElement = (Element) xpath.selectSingleNode(element);
        column.setColumnName(subElement.getAttributeValue("name"));
        column.setMetaData(this.parseColumnMetaData(element, subElement, ColumnMetaData.IndexAttribute.PrimaryKey));
        columns.add(column);
    }
    
    private void    parseClass(TableDefinition definition, Element rootElement) throws JDOMException, ClassNotFoundException
    {
        XPath       xpath = XPath.newInstance("//class");
        Element     element;
        
        element = (Element) xpath.selectSingleNode(rootElement);
        definition.setTableName(element.getAttributeValue("table"));
        definition.setObjectClassForName(element.getAttributeValue("name"));
    }
    
    private void      parseColumns(List<Column> columns, Element rootElement) throws JDOMException
    {
        XPath           xpath = XPath.newInstance("//property");
        List<Element>   elements = null;
        
        elements = xpath.selectNodes(rootElement);
        for (Element element : elements)
            columns.add(this.parseColumn(element));
    }
    
    private Column    parseColumn(Element element) throws JDOMException
    {
        Column  column = new Column();
        XPath   xpath = XPath.newInstance("./column");
        Element subElement = null;
        
        column.setObjectPropertyName(element.getAttributeValue("name"));
        
        subElement = (Element) xpath.selectSingleNode(element);
        
        column.setColumnName(subElement.getAttributeValue("name"));
        column.setMetaData(this.parseColumnMetaData(element, subElement, ColumnMetaData.IndexAttribute.Default));
        if (subElement.getAttribute("not-null") != null)
        {
            column.getMetaData().setIsNullable(Boolean.parseBoolean(subElement.getAttributeValue("not-null")));
        }
        return column;
    }
    
    private ColumnMetaData  parseColumnMetaData(Element propertyElement, Element columnElement, ColumnMetaData.IndexAttribute attribute)
    {
        XMLColumnMetaDataParser metaDataParser = new XMLColumnMetaDataParser(propertyElement, columnElement, attribute);
        
        return (metaDataParser.parse());
    }
}
    