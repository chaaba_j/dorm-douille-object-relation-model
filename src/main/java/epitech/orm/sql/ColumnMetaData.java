/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epitech.orm.sql;

/**
 *
 * @author jalal
 */
public class ColumnMetaData 
{

    public enum ColumnType
    {
        String,
        Integer,
        Date,
        Float,
        Char
    }

    public enum IndexAttribute
    {
      Default,
      PrimaryKey,
      UniqueKey,
      ForeignKey
    }
    
    public enum DefaultValueAttribute
    {
      Nullable,
      AsDefined,
      None,
      CurrentTimeStamp
    };
    
    private static int  UNSPECIFIED_LENGTH = -1;
    
    private DefaultValueAttribute   defaultAttribute;
    private IndexAttribute          _indexAttribute;
    private boolean                 _autoIncrement = false;
    private boolean                 _isNullable = true;
    private int                     _length = UNSPECIFIED_LENGTH;
    private Object                  _defaultValue = null;
    private ColumnType              _type;
    
    public ColumnMetaData()
    {
        
    }
    
    public IndexAttribute   getIndexAttribute()
    {
        return this._indexAttribute;
    }
    
    public void             setIndexAttribute(IndexAttribute attr)
    {
        this._indexAttribute = attr;
    }
    
    
        
    public void setDefaultValue(Object value)
    {
        this._defaultValue = value;
    }
    
    public Object getDefaultValue()
    {
        return this._defaultValue;
    }
    /**
     * @return the _autoIncrement
     */
    public boolean isAutoIncrement() {
        return _autoIncrement;
    }

    /**
     * @param autoIncrement the _autoIncrement to set
     */
    public void setAutoIncrement(boolean autoIncrement) {
        this._autoIncrement = autoIncrement;
    }

    /**
     * @return the _isNullable
     */
    public boolean isIsNullable() {
        return _isNullable;
    }

    /**
     * @param isNullable the _isNullable to set
     */
    public void setIsNullable(boolean isNullable) {
        this._isNullable = isNullable;
    }

    /**
     * @return the _length
     */
    public int getLength() {
        return _length;
    }

    /**
     * @param length the _length to set
     */
    public void setLength(int length) {
        this._length = length;
    }
    
    
    /**
     * @return the defaultAttribute
     */
    public DefaultValueAttribute getDefaultAttribute() {
        return defaultAttribute;
    }

    /**
     * @param defaultAttribute the defaultAttribute to set
     */
    public void setDefaultAttribute(DefaultValueAttribute defaultAttribute) {
        this.defaultAttribute = defaultAttribute;
    }

    /**
     * @return the _type
     */
    public ColumnType getType() {
        return _type;
    }

    /**
     * @param type the _type to set
     */
    public void setType(ColumnType type) {
        this._type = type;
    }
}
