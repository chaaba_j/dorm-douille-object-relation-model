/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epitech.orm.sql;

/**
 *
 * @author jalal
 */
public class Column 
{
    private String              _columnName;
    private String              _sqlGeneratorFunction;
    private String              _objectPropertyName;
    private SQLRelation         _relation;
    private ColumnMetaData      _metaData;
    
    public Column()
    {
        
    }
          
    /**
     * @return the _sqlGeneratorFunction
     */
    public String getSqlGeneratorFunction() 
    {
        return _sqlGeneratorFunction;
    }

    /**
     * @param sqlGeneratorFunction the _sqlGeneratorFunction to set
     */
    public void setSqlGeneratorFunction(String sqlGeneratorFunction) 
    {
        this._sqlGeneratorFunction = sqlGeneratorFunction;
    }
    
    /**
     * @return the _columnName
     */
    public String getColumnName() 
    {
        return _columnName;
    }

    /**
     * @param columnName the _columnName to set
     */
    public void setColumnName(String columnName) 
    {
        this._columnName = columnName;
    }

        /**
     * @return the _relation
     */
    public SQLRelation getRelation() 
    {
        return _relation;
    }

    /**
     * @param relation the _relation to set
     */
    public void setRelation(SQLRelation relation) 
    {
        this._relation = relation;
    }
    
        /**
     * @return the _objectPropertyName
     */
    public String getObjectPropertyName() 
    {
        return _objectPropertyName;
    }

    public void setObjectPropertyName(String propertyName)
    {
        this._objectPropertyName = propertyName;
    }

    /**
     * @return the _metaData
     */
    public ColumnMetaData getMetaData() {
        return _metaData;
    }

    /**
     * @param metaData the _metaData to set
     */
    public void setMetaData(ColumnMetaData metaData) {
        this._metaData = metaData;
    }
}
