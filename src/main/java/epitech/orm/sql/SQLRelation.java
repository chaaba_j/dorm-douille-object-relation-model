/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epitech.orm.sql;

import epitech.orm.configuration.TableDefinition;

/**
 *
 * @author jalal
 */
public class SQLRelation 
{
    /**
     * @return the _propertyName
     */
    public String getPropertyName() {
        return _propertyName;
    }

    /**
     * @param propertyName the _propertyName to set
     */
    public void setPropertyName(String propertyName) {
        this._propertyName = propertyName;
    }


    public enum RelationType
    {
        OneToMany,
        ManyToMany,
        OneToOne,
        ManyToOne
    };
    
    private String          _targetSymbol;
    private String          _propertyName;
    private RelationType    _relationType;
    private TableDefinition _tableDefinition;
    
    public SQLRelation()
    {
        
    }
    
    /**
     * @return the _targetSymbol
     */
    public String getTargetSymbol() 
    {
        return _targetSymbol;
    }

    /**
     * @param targetSymbol the _targetSymbol to set
     */
    public void setTargetSymbol(String targetSymbol) 
    {
        this._targetSymbol = targetSymbol;
    }

    /**
     * @return the _tableDefinition
     */
    public TableDefinition getTableDefinition() 
    {
        return _tableDefinition;
    }

    /**
     * @param tableDefinition the _tableDefinition to set
     */
    public void setTableDefinition(TableDefinition tableDefinition) {
        this._tableDefinition = tableDefinition;
    }

    /**
     * @return the _relationType
     */
    public RelationType getRelationType() {
        return _relationType;
    }

    /**
     * @param relationType the _relationType to set
     */
    public void setRelationType(RelationType relationType) {
        this._relationType = relationType;
    }
    
}
