/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epitech.orm.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author jalal
 */
public class Restriction<T>
{

    public enum RestrictionType
    {
        Equal,
        Lower,
        Greater,
        Different,
        Like,
        LowerOrEqual,
        GreaterOrEqual,
        In
    }
    
    public enum RestrictionTag
    {
      And,
      Or,
    };

    
    private RestrictionType     _type;
    private T                   _value;
    private RestrictionTag      _tag;
    private List<Restriction>   _nestedRestriction = new ArrayList<>();
    private String              _columnName;

    /**
     * @return the _type
     */
    public RestrictionType getType() 
    {
        return _type;
    }

    public List<Restriction>    getNestedRestriction()
    {
        return this._nestedRestriction;
    }
    
    /**
     * @return the _value
     */
    public T getValue() 
    {
        return _value;
    }

    public Restriction and(Restriction restriction)
    {
        if (this._nestedRestriction.isEmpty())
            this._tag = RestrictionTag.And;
        else
            this._nestedRestriction.get(this._nestedRestriction.size() - 1).setRestrictionTag(RestrictionTag.And);
        this._nestedRestriction.add(restriction);
        return this;
    }
    
    public Restriction or(Restriction restriction)
    {
        if (this._nestedRestriction.isEmpty())
            this._tag = RestrictionTag.Or;
        else
            this._nestedRestriction.get(this._nestedRestriction.size() - 1).setRestrictionTag(RestrictionTag.Or);
        this._nestedRestriction.add(restriction);
        return this;
    }
    
    public RestrictionTag   getRestrictionTag()
    {
        return this._tag;
    }
    
    public void             setRestrictionTag(RestrictionTag tag)
    {
        this._tag = tag;
    }
    
    /**
     * @return the _columnName
     */
    public String getColumnName()
    {
        return _columnName;
    }
    
    private Restriction(RestrictionType type, String columnName, T value)
    {
        this._type = type;
        this._columnName = columnName;
        this._value = value;
    }
    
   
    
    public static <T> Restriction<T>   equal(String columnName, T value)
    {
       return new Restriction<>(RestrictionType.Equal, columnName, value);
    }
    
    public static <T> Restriction<T>   lower(String columnName, T value)
    {
        return new Restriction<>(RestrictionType.Lower, columnName, value);
    }
    
    public static <T> Restriction<T>   greater(String columnName, T value)
    {
        return new Restriction<>(RestrictionType.Greater, columnName, value);
    }
    
    public static <T> Restriction<T>   notEqual(String columnName, T value)
    {
        return new Restriction<>(RestrictionType.Different, columnName, value);
    }
    
    public static Restriction<String>   like(String columnName, String pattern)
    {
        return new Restriction<>(RestrictionType.Like, columnName, pattern);
    }
    
    public static <T> Restriction<T>   lowerOrEqual(String columnName, T value)
    {
        return new Restriction<>(RestrictionType.LowerOrEqual, columnName, value);
    }
    
    public static <T> Restriction<T>   greaterOrEqual(String columnName, T value)
    {
        return new Restriction<>(RestrictionType.GreaterOrEqual, columnName, value);
    }
    
    public static <T> Restriction< Collection<T> >    in(String columnName, Collection<T> value)
    {
        return new Restriction< >(RestrictionType.In, columnName, value);
    }
    
    public static <T> Restriction<T[]>              in(String columnName, T[] value)
    {
        return new Restriction<>(RestrictionType.In, columnName, value);
    }
}
