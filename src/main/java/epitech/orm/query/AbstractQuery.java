/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epitech.orm.query;

import epitech.orm.configuration.TableDefinition;
import java.sql.Connection;
import java.sql.SQLException;

/**
 *
 * @author jalal
 */
public abstract class AbstractQuery
{
    protected TableDefinition   _definition;
    protected Connection        _connection;  
    
    public AbstractQuery(Connection connection, TableDefinition definition)
    {
        this._definition = definition;
        this._connection = connection;
    }
    
    abstract public <T> T execute() throws SQLException;
}
