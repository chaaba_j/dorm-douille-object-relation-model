/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epitech.orm.query;

import epitech.orm.configuration.TableDefinition;
import java.sql.Connection;
import java.sql.SQLException;

/**
 *
 * @author jalal
 */
public abstract class AbstractQueryFilterable extends AbstractQuery
{
    protected WhereClause     _whereClause;

    public AbstractQueryFilterable(Connection connection, TableDefinition definition) 
    {
        super(connection, definition);
    }

    public AbstractQueryFilterable  where(Restriction restriction)
    {
        if (this._whereClause == null)
            this._whereClause = new WhereClause(restriction);
        return this;
    }
    
    public AbstractQueryFilterable  and(Restriction restriction)
    {
        this._whereClause.and(restriction);
        return this;
    }
    
    public  AbstractQueryFilterable  or(Restriction restriction)
    {
        this._whereClause.or(restriction);
        return this;
    }
    
    @Override
    abstract public <T> T execute() throws SQLException;
}
