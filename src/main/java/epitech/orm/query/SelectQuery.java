/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epitech.orm.query;

import epitech.orm.configuration.TableDefinition;
import epitech.orm.query.builder.ISQLSelectBuilder.Order;
import epitech.orm.query.builder.SQLSelectBuilder;
import epitech.orm.sql.Column;
import epitech.orm.sql.ColumnMetaData;
import epitech.orm.sql.SQLRelation;
import epitech.orm.utils.ReflectUtils;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jalal
 */
public class SelectQuery extends AbstractQueryFilterable
{   
    private SQLSelectBuilder    _selectBuilder;
    
    public SelectQuery(Connection connection, TableDefinition definition)
    {
        super(connection, definition);
        this._selectBuilder = new SQLSelectBuilder(definition);
    }
    
    private Object   getValue(Column column, ResultSet result) throws SQLException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException
    {
        switch (column.getMetaData().getType())
        {
            case Integer:
                return result.getInt(column.getColumnName());
            case String:
                return result.getString(column.getColumnName());
            case Float:
                return result.getFloat(column.getColumnName());
            case Char:
                return result.getByte(column.getColumnName());
            case Date:
                return result.getDate(column.getColumnName());
        }
        return null;
    }
    
    private List<Object>    retreiveObjects(ResultSet result, Object obj, Column column) throws NoSuchFieldException, IllegalAccessException, IllegalAccessException, IllegalArgumentException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SQLException
    {
        List<Object>        results;
        Object              keyObj;
        SelectQuery         dependancyQuery = new SelectQuery(_connection, column.getRelation().getTableDefinition());
        
        if (column.getRelation().getRelationType() == SQLRelation.RelationType.OneToMany 
            || column.getRelation().getRelationType() == SQLRelation.RelationType.OneToOne)
        {
            keyObj = ReflectUtils.getAttribute(obj, column.getRelation().getPropertyName());
            dependancyQuery.where(Restriction.equal(column.getColumnName(), keyObj));
            results = dependancyQuery.execute().list();
        }
        else
        {
            keyObj = this.getValue(column, result);
            dependancyQuery.where(Restriction.equal(column.getRelation().getTableDefinition().getPrimaryKey().getColumnName(), keyObj));
            results = dependancyQuery.execute().list();
        }
        return results;
    }
    
    private void     setDependancy(ResultSet result, Object obj, Column column) throws SQLException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, NoSuchFieldException
    {
        
       List<Object>        results = this.retreiveObjects(result, obj, column);
    
       if (results.size() > 0)
       {
            switch (column.getRelation().getRelationType())
            {
                case OneToMany:
                    ReflectUtils.setAttribute(obj, column.getObjectPropertyName(), results);
                    break;
                case OneToOne:
                    ReflectUtils.setAttribute(obj, column.getObjectPropertyName(), results.get(0));
                    break;
                case ManyToOne:
                    ReflectUtils.setAttribute(obj, column.getObjectPropertyName(), results.get(0));
                    break;
                default:
                    break;
            }
       }
    }
    
    private Object   makeObject(ResultSet result) throws SQLException
    {
        try 
        {
            Object      obj = ReflectUtils.makeInstance(this._definition.getObjectClass());
            
            for (Column column : this._definition.getColumns())
            {
                if (column.getRelation() == null)
                    ReflectUtils.setAttribute(obj, column.getObjectPropertyName(), this.getValue(column, result));
                else
                {
                    this.setDependancy(result, obj, column);
                }
            }
            return obj;
        }  
        catch (NoSuchMethodException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException  | NoSuchFieldException ex) 
        {
            Logger.getLogger(SelectQuery.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }
        catch (SQLException ex) 
        {
            throw new SQLException(ex.getMessage());
        }
        return null;
    }
    
    private List<Object> resultsToObject(ResultSet result) throws SQLException
    {
        List<Object> objs = new ArrayList<>();
        
        while (result.next())
        {
            objs.add(this.makeObject(result));
        }
        return objs;
    }
       
    public SelectQuery  limit(int start, int end)
    {
        _selectBuilder.limit(start, end);
        return this;
    }
    
    public SelectQuery  sort(Order order, String columnName)
    {
        _selectBuilder.sort(order, columnName);
        return this;
    }
    
    public SelectQuery  first()
    {
        _selectBuilder.limit(0, 1);
        return this;
    }
    
    public SelectQuery last()
    {
        _selectBuilder.limit(0, 1).sort(Order.DESC, "1");
        return this;
    }
    
    @Override
    public SelectQuery  where(Restriction restriction)
    {
        super.where(restriction);
        return this;
    }
    
    @Override
    public SelectQuery  and(Restriction restriction)
    {
        super.and(restriction);
        return this;
    }
    
    @Override
    public SelectQuery  or(Restriction restriction)
    {
        super.or(restriction);
        return this;
    }
    
    
    @Override
    public Result execute() throws SQLException
    {
        String              query;
        Statement           statement = null;
        ResultSet           result = null;
        
        this._selectBuilder.select();
        
        if (this._whereClause != null)
            _selectBuilder.restrict(this._whereClause.getRestrictions());
        try 
        {
            query = this._selectBuilder.build();
            
            statement = this._connection.createStatement();
            result = statement.executeQuery(query);
            return new Result(this.resultsToObject(result));
        } 
        catch (SQLException ex) 
        {
            throw new SQLException(ex.getMessage());
        }
        finally
        {
            if (result != null)
            {
                try 
                {
                    result.close();
                } 
                catch (SQLException ex) 
                {
                    Logger.getLogger(SelectQuery.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
                }
            }
            if (statement != null)
            {
                    try 
                    {
                        statement.close();
                    } 
                    catch (SQLException ex) 
                    {
                        Logger.getLogger(SelectQuery.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
                    }
            }
        }
    }
}
