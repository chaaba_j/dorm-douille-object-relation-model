/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epitech.orm.query.builder;

import epitech.orm.sql.Column;
import epitech.orm.sql.ColumnMetaData;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author jalal
 */
public class SQLColumnBuilder implements ISQLColumnBuilder
{
    private static final Map<ColumnMetaData.ColumnType, String>   _typesConverter = new HashMap();
    static 
    {
        _typesConverter.put(ColumnMetaData.ColumnType.Char, "CHAR");
        _typesConverter.put(ColumnMetaData.ColumnType.Float, "FLOAT");
        _typesConverter.put(ColumnMetaData.ColumnType.Integer, "INTEGER");
        _typesConverter.put(ColumnMetaData.ColumnType.String, "TEXT");
        _typesConverter.put(ColumnMetaData.ColumnType.Date, "DATETIME");
    };
    
    private static final Map<ColumnMetaData.DefaultValueAttribute, String> _attributeDefaultConverter = new HashMap();
    static
    {
      _attributeDefaultConverter.put(ColumnMetaData.DefaultValueAttribute.None, "NOT NULL");
      _attributeDefaultConverter.put(ColumnMetaData.DefaultValueAttribute.Nullable, "DEFAULT NULL");
      _attributeDefaultConverter.put(ColumnMetaData.DefaultValueAttribute.AsDefined, "DEFAULT ");
      _attributeDefaultConverter.put(ColumnMetaData.DefaultValueAttribute.CurrentTimeStamp, "DEFAULT CURRENT_TIMESTAMP");
    };
    
    private static final Map<ColumnMetaData.IndexAttribute, String> _indexesConverter = new HashMap();
    static
    {
        _indexesConverter.put(ColumnMetaData.IndexAttribute.UniqueKey, "UNIQUE KEY");
        _indexesConverter.put(ColumnMetaData.IndexAttribute.PrimaryKey, "PRIMARY KEY");
    };

    
    private StringBuilder   _builder = new StringBuilder();
    private Column          _column;
    
    public SQLColumnBuilder(Column column)
    {
        this._column = column;
    }
    
    private String  buildDefaultValue()
    {
        StringBuilder   defaultValueBuilder = new StringBuilder();
        
        defaultValueBuilder.append(_attributeDefaultConverter.get(this._column.getMetaData().getDefaultAttribute())).append(" ");
        defaultValueBuilder.append(this._column.getMetaData().getDefaultValue());
        defaultValueBuilder.append(SQLValueTransformer.transform(this._column.getMetaData().getDefaultValue()));
        return defaultValueBuilder.toString();
    }
    
    public String buildColumn()
    {
        _builder.setLength(0);
        _builder.append(this._column.getColumnName()).append(" ");
        if (this._column.getMetaData().getType() == ColumnMetaData.ColumnType.String 
            && this._column.getMetaData().getLength() != -1)
            _builder.append("VARCHAR(").append(this._column.getMetaData().getLength()).append(") ");
        else
        {
            _builder.append(_typesConverter.get(this._column.getMetaData().getType())).append(" ");
            if (this._column.getMetaData().getDefaultAttribute() != ColumnMetaData.DefaultValueAttribute.AsDefined)
                _builder.append(_attributeDefaultConverter.get(this._column.getMetaData().getDefaultAttribute()));
            else
                _builder.append(this.buildDefaultValue());
        }
        if (this._column.getMetaData().isAutoIncrement())
            _builder.append(" AUTO_INCREMENT ");
        return _builder.toString();
    }

    public String buildIndexes() 
    {
        _builder.setLength(0);
        _builder.append(_indexesConverter.get(this._column.getMetaData().getIndexAttribute())).append(" ");
        _builder.append("(").append(this._column.getColumnName()).append(")");
        
        return _builder.toString();
    }

    public Column getColumn() 
    {
        return this._column;
    }
    
}
