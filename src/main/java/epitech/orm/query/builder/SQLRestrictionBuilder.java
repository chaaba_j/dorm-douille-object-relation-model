/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epitech.orm.query.builder;

import epitech.orm.query.Restriction;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author ledinh
 */
public class SQLRestrictionBuilder implements ISQLRestrictionBuilder
{
    private static final Map<Restriction.RestrictionType, String>   _sqlRestrictionTypes = new EnumMap<>(Restriction.RestrictionType.class);
    static
    {
        _sqlRestrictionTypes.put(Restriction.RestrictionType.Like, "LIKE");
        _sqlRestrictionTypes.put(Restriction.RestrictionType.Different, "<>");
        _sqlRestrictionTypes.put(Restriction.RestrictionType.Equal, "=");
        _sqlRestrictionTypes.put(Restriction.RestrictionType.Greater, ">");
        _sqlRestrictionTypes.put(Restriction.RestrictionType.GreaterOrEqual, ">=");
        _sqlRestrictionTypes.put(Restriction.RestrictionType.Lower, "<");
        _sqlRestrictionTypes.put(Restriction.RestrictionType.LowerOrEqual, "<=");
        _sqlRestrictionTypes.put(Restriction.RestrictionType.In, "IN");
    };
    
    private static final Map<Restriction.RestrictionTag, String>    _sqlRestrictionTag = new EnumMap<>(Restriction.RestrictionTag.class);
    static
    {
      _sqlRestrictionTag.put(Restriction.RestrictionTag.And, "AND");
      _sqlRestrictionTag.put(Restriction.RestrictionTag.Or, "OR");
    };
    
    private StringBuilder           _builder = new StringBuilder();
    private List<Restriction>       _restrictions;
    
    public SQLRestrictionBuilder(List<Restriction> restriction)
    {
        this._restrictions = restriction;
    }
    
    @Override
    public String build() 
    {
        ISQLRestrictionBuilder  restrictionBuilder;
        
        _builder.setLength(0);
        _builder.append('(');
        for (Restriction restriction : this._restrictions)
        {
  
            if (!restriction.getNestedRestriction().isEmpty())
                _builder.append('(');
            _builder.append(restriction.getColumnName())
                    .append(" ")
                    .append(_sqlRestrictionTypes.get(restriction.getType()))
                    .append(" ")
                    .append(SQLValueTransformer.transform(restriction.getValue())).append(" ");
            if (restriction.getRestrictionTag() != null)
            {
                _builder.append(_sqlRestrictionTag.get(restriction.getRestrictionTag()));
                _builder.append(" ");
            }
            if (!restriction.getNestedRestriction().isEmpty())
            {
                restrictionBuilder = new SQLRestrictionBuilder(restriction.getNestedRestriction());
                _builder.append(restrictionBuilder.build());
            }
            if (!restriction.getNestedRestriction().isEmpty())
                _builder.append(')');
        }
        _builder.append(')');
        return _builder.toString();
    }
}
