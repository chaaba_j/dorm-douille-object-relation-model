/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epitech.orm.query.builder;

import epitech.orm.configuration.TableDefinition;

/**
 *
 * @author jalal
 */
public class BaseQueryBuilder 
{
    protected StringBuilder     _builder = new StringBuilder();
    protected TableDefinition   _definition;
    
    public BaseQueryBuilder(TableDefinition definition)
    {
        this._definition = definition;
    }
    
}
