/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epitech.orm.query.builder;

import epitech.orm.query.Restriction;
import java.util.List;

/**
 *
 * @author ledinh
 */
public interface ISQLSaveBuilder 
{
    public <T> ISQLSaveBuilder      update(String columnName, T value);
    public     ISQLSaveBuilder      restrict(List<Restriction> restrictions);
    public <T> ISQLSaveBuilder      set(String columnName, T value);
    public String                   build();
}
