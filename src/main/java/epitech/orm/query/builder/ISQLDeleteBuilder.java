/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epitech.orm.query.builder;

import epitech.orm.query.Restriction;
import java.util.List;

/**
 *
 * @author ledinh
 */
public interface ISQLDeleteBuilder 
{
    public ISQLDeleteBuilder    delete();
    public ISQLDeleteBuilder    restrict(List<Restriction> restrictions);
    public String               build();
}
