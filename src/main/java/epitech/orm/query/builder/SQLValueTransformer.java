/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epitech.orm.query.builder;

import java.util.Collection;

/**
 *
 * @author jalal
 */
public class SQLValueTransformer 
{
    
    public static <T> String    transform(T object)
    {
        if (object == null)
            return "NULL";
        else if (object instanceof Collection)
        {
            return SQLValueTransformer.transformCollection((Collection<T>)object);
        }
        else if (object.getClass().isArray())
        {
            return SQLValueTransformer.transformArray((T[])object);
        }
        else if (object instanceof String)
        {
            return SQLValueTransformer.transformString((String)object);
        }
        else if (object.getClass().getName().compareTo("C") == 0)
        {
            return SQLValueTransformer.transformChar((Character)object);
        }
        return object.toString();
    }
    
    private static String   transformString(String str)
    {
        StringBuilder       builder = new StringBuilder();
        
        return builder.append('"').append(str).append('"').toString();
    }
    
    private static String   transformChar(Character c)
    {
        StringBuilder       builder = new StringBuilder();
        
        return builder.append("'").append(c).append("'").toString();
    }
    
    private static <T> String   transformCollection(Collection<T> collection)
    {
        StringBuilder           builder = new StringBuilder();
        
        builder.append('(');
        for (T obj : collection)
        {
            builder.append(SQLValueTransformer.transform(obj)).append(',');
        }
        builder.deleteCharAt(builder.length() - 1).append(')');
        return builder.toString();
    }
    
    private static <T> String   transformArray(T[] objects)
    {
        StringBuilder           builder = new StringBuilder();
        
        builder.append('(');
        for (T obj : objects)
        {
            builder.append(SQLValueTransformer.transform(obj)).append(',');
        }
        builder.deleteCharAt(builder.length() - 1).append(')');
        return builder.toString();
    }
}
