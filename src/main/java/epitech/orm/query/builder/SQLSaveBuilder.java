/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epitech.orm.query.builder;

import epitech.orm.configuration.TableDefinition;
import epitech.orm.query.Restriction;
import java.util.List;

/**
 *
 * @author ledinh
 */
public class SQLSaveBuilder extends BaseQueryBuilder implements ISQLSaveBuilder
{  
    
    public SQLSaveBuilder(TableDefinition definition)
    {
        super(definition);
    }
    
    public <T> ISQLSaveBuilder update(String columnName, T value) 
    {
        _builder.setLength(0);
        _builder.append("UPDATE ").append(_definition.getTableName());
        _builder.append(" SET ").append(columnName).append("=").append(SQLValueTransformer.transform(value));
        return this;
    }

    public ISQLSaveBuilder restrict(List<Restriction> restrictions) 
    {
        ISQLRestrictionBuilder  restrictionBuilder = new SQLRestrictionBuilder(restrictions);
        
        _builder.append(" WHERE ").append(restrictionBuilder.build());
        return this;
    }

    public <T> ISQLSaveBuilder set(String columnName, T value) 
    {
        _builder.append(", ").append(columnName).append("=").append(SQLValueTransformer.transform(value));
        return this;
    }

    public String build() 
    {
        return _builder.toString();
    }
    
}
