/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epitech.orm.query.builder;

import epitech.orm.configuration.TableDefinition;
import epitech.orm.query.Restriction;
import java.util.List;


/**
 *
 * @author ledinh
 */
public interface ISQLSelectBuilder 
{
    public enum Order
    {
      ASC,
      DESC
    }; 
    
    public ISQLSelectBuilder    select();
    public ISQLSelectBuilder    restrict(List<Restriction> restrictions);
    public ISQLSelectBuilder    limit(int start, int nbElement);
    public ISQLSelectBuilder    sort(Order order, String columnName);
    public String               build();
    public TableDefinition      getTableDefinition();
}
