/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epitech.orm.query.builder;

import epitech.orm.configuration.TableDefinition;
import epitech.orm.query.Restriction;
import java.util.List;

/**
 *
 * @author ledinh
 */
public class SQLDeleteBuilder extends BaseQueryBuilder implements ISQLDeleteBuilder
{
    
    public SQLDeleteBuilder(TableDefinition definition)
    {
        super(definition);
    }
    
    public String build() 
    {
       _builder.append(";");
       return _builder.toString();
    }

    public ISQLDeleteBuilder delete() 
    {
        _builder.setLength(0);
       _builder.append("DELETE FROM ").append(_definition.getTableName()).append(" ");
       return this;
    }

    public ISQLDeleteBuilder restrict(List<Restriction> restrictions) 
    {
        ISQLRestrictionBuilder      restrictionBuilder = new SQLRestrictionBuilder(restrictions);
        
        _builder.append("WHERE ").append(restrictionBuilder.build());
        return this;
    }
 
}
