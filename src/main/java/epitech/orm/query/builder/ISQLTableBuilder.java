/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epitech.orm.query.builder;

import epitech.orm.sql.Column;

/**
 *
 * @author ledinh
 */
public interface ISQLTableBuilder 
{
   
    public ISQLTableBuilder     create(String tableName) throws Exception;
    public void                 addField(Column culumn);
    public String               build() throws Exception;
}
