/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epitech.orm.query.builder;

import epitech.orm.exception.IncompleteSQLRequestException;

/**
 *
 * @author ledinh
 */
public interface ISQLInsertBuilder 
{
    public     ISQLInsertBuilder    insert();
    public <T> ISQLInsertBuilder    forColumn(String columnName, T value);
    public String                   build() throws IncompleteSQLRequestException;
}
