/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epitech.orm.query.builder;

import epitech.orm.configuration.TableDefinition;
import epitech.orm.query.Restriction;
import java.util.List;


/**
 *
 * @author ledinh
 */
public interface ISQLQueryBuilder 
{
    public ISQLQueryBuilder select();
    public ISQLQueryBuilder restrict(List<Restriction> restrictions);
    public String           build();
    public TableDefinition  getTableDefinition();
}
