/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epitech.orm.query.builder;

import epitech.orm.configuration.TableDefinition;
import epitech.orm.query.Restriction;
import java.util.List;

/**
 *
 * @author ledinh
 */
public class SQLQueryBuilder<T> implements ISQLQueryBuilder
{
    private StringBuilder               _builder = new StringBuilder();
    private TableDefinition             _definition;
    private String                      _query;
    
    public SQLQueryBuilder(TableDefinition definition)
    {
        this._definition = definition;
    }
    
    public ISQLQueryBuilder select() 
    {
        _builder.append("SELECT * FROM ").append(_definition.getTableName());
        return this;
    }

    public String build() 
    {
        String  query;
        
        _builder.append(";");
        query = _builder.toString();
        _builder.setLength(0);
        return query;
    }
    
    public TableDefinition getTableDefinition()
    {
        return this._definition;
    }

    public ISQLQueryBuilder restrict(List<Restriction> restrictions) 
    {
        ISQLRestrictionBuilder      restrictionBuilder = new SQLRestrictionBuilder(restrictions);
        
        _builder.append(" WHERE ");
        _builder.append(restrictionBuilder.build());
        return this;
    }
}
