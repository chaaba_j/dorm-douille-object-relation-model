/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epitech.orm.query.builder;

import epitech.orm.sql.Column;

/**
 *
 * @author ledinh
 */
public interface ISQLColumnBuilder 
{
    public String   buildColumn();
    public String   buildIndexes();
    public Column   getColumn();
}
