/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epitech.orm.query.builder;

import epitech.orm.configuration.Configuration;
import epitech.orm.sql.Column;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ledinh
 */
public class SQLTableBuilder implements ISQLTableBuilder
{
    private boolean         _isCreated = false;
    private StringBuilder   _queryBuilder = new StringBuilder();
    private List<String>    _columnCompiled = new ArrayList<>();
    private List<String>    _indexesCompiled = new ArrayList<>();
    
    public SQLTableBuilder()
    {
        
    }

    @Override
    public ISQLTableBuilder create(String tableName) throws Exception 
    {
        if (this._isCreated == true)
            throw new Exception("Cannot create table is if already created");
        this._isCreated = true;
        _queryBuilder.append("CREATE TABLE IF NOT EXISTS ");
        _queryBuilder.append(tableName);
        return this;
    }

    @Override
    public void addField(Column culumn) 
    {
        ISQLColumnBuilder   columnBuilder = new SQLColumnBuilder(culumn);
        
        this._columnCompiled.add(columnBuilder.buildColumn());
        this._columnCompiled.add(columnBuilder.buildIndexes());
    }

    @Override
    public String build() throws Exception 
    {
        String  query;
        
        if (this._isCreated == false)
            throw new Exception("Cannot build a non created table");
        _queryBuilder.append("(");
        this.buildColumns();
        this.buildIndexes();
        if (_queryBuilder.indexOf(",") != -1)
            _queryBuilder.deleteCharAt(_queryBuilder.lastIndexOf(","));
        _queryBuilder.append(')');
        _queryBuilder.append("ENGINE=").append(Configuration.sqlEngine);
        _queryBuilder.append(" DEFAULT ");
        _queryBuilder.append("CHARSET=").append(Configuration.charset);
        return _queryBuilder.toString();
    }
    
    private void    buildColumns()
    {
        for (String column : this._columnCompiled)
        {
            _queryBuilder.append(column).append(",");
        }
    }
    
    private void    buildIndexes()
    {
        for (String indexCompiled : this._indexesCompiled)
        {
            _queryBuilder.append(indexCompiled).append(",");
        }
    }
}
