/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epitech.orm.query.builder;

import epitech.orm.configuration.TableDefinition;
import epitech.orm.exception.IncompleteSQLRequestException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 *
 * @author ledinh
 */
public class SQLInsertBuilder extends BaseQueryBuilder implements ISQLInsertBuilder
{
    private Map<String, Object>         _datas = new HashMap<>();
    
    public SQLInsertBuilder(TableDefinition definition)
    {
        super(definition);
    }
    
    @Override
    public ISQLInsertBuilder insert() 
    {
        _builder.setLength(0);
        _datas.clear();
        _builder.append("INSERT INTO ").append(_definition.getTableName()).append(" ");
        return this;
    }

    @Override
    public <T> ISQLInsertBuilder forColumn(String column, T value) 
    {
        _datas.put(column, value);
        return this;
    }
    
    @Override
    public String build() throws IncompleteSQLRequestException 
    {
        StringBuilder   columnBuilder = new StringBuilder();
        StringBuilder   valuesBuilder = new StringBuilder();
        //TODO check all required data is in map
        if (_datas.isEmpty())
            throw new IncompleteSQLRequestException("Fail to build insert query : Your insert request is incomplete please insert data");
        columnBuilder.append('(');
        valuesBuilder.append('(');
        for (Entry<String, Object> entry : _datas.entrySet())
        {
            columnBuilder.append(entry.getKey()).append(',');
            valuesBuilder.append(SQLValueTransformer.transform(entry.getValue())).append(',');
        }
        columnBuilder.deleteCharAt(columnBuilder.length() - 1).append(')');
        valuesBuilder.deleteCharAt(valuesBuilder.length() - 1).append(')');
        _builder.append(columnBuilder).append(" VALUES ");
        _builder.append(valuesBuilder).append(" ");
        _builder.append(';');
        return _builder.toString();
    }
}