/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epitech.orm.query.builder;

/**
 *
 * @author ledinh
 */
public interface ISQLRestrictionBuilder 
{
    public String                       build();
}
