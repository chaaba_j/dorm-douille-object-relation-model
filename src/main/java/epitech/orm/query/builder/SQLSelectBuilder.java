/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epitech.orm.query.builder;

import epitech.orm.configuration.TableDefinition;
import epitech.orm.query.Restriction;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author ledinh
 */
public class SQLSelectBuilder<T> extends BaseQueryBuilder implements ISQLSelectBuilder 
{
    public enum PriorityOrder
    {
        Where,
        OrderBy,
        Limit
    };
    
    private HashMap<PriorityOrder, StringBuilder>   _priorityBuilder = new HashMap();
    
    public SQLSelectBuilder(TableDefinition definition)
    {
        super(definition);
        this._priorityBuilder.put(SQLSelectBuilder.PriorityOrder.Where, new StringBuilder());
        this._priorityBuilder.put(SQLSelectBuilder.PriorityOrder.Limit, new StringBuilder());
        this._priorityBuilder.put(SQLSelectBuilder.PriorityOrder.OrderBy, new StringBuilder());
    }
    
    @Override
    public ISQLSelectBuilder select() 
    {
        _builder.setLength(0);
        _builder.append("SELECT * FROM ").append(_definition.getTableName());
        return this;
    }

    @Override
    public String build() 
    {
        _builder.append(_priorityBuilder.get(SQLSelectBuilder.PriorityOrder.Where));
        _builder.append(_priorityBuilder.get(SQLSelectBuilder.PriorityOrder.OrderBy));
        _builder.append(_priorityBuilder.get(SQLSelectBuilder.PriorityOrder.Limit));
        return _builder.toString();
    }
    
    @Override
    public TableDefinition getTableDefinition()
    {
        return this._definition;
    }

    @Override
    public ISQLSelectBuilder restrict(List<Restriction> restrictions) 
    {
        ISQLRestrictionBuilder      restrictionBuilder = new SQLRestrictionBuilder(restrictions);
        StringBuilder               whereBuilder;
        
        whereBuilder = _priorityBuilder.get(SQLSelectBuilder.PriorityOrder.Where);
        whereBuilder.setLength(0);
        whereBuilder.append(" WHERE ");
        whereBuilder.append(restrictionBuilder.build());
        return this;
    }

    @Override
    public ISQLSelectBuilder limit(int start, int nbElement) 
    {
        StringBuilder               limitBuilder;
        
        limitBuilder = _priorityBuilder.get(SQLSelectBuilder.PriorityOrder.Limit);
        limitBuilder.setLength(0);
        limitBuilder.append(" LIMIT ").append(start).append(", ").append(nbElement);
        
        return this;
    }
    

    @Override
    public ISQLSelectBuilder sort(Order order, String columnName) 
    {
        StringBuilder               sortBuilder;
        
        sortBuilder = _priorityBuilder.get(SQLSelectBuilder.PriorityOrder.OrderBy);
        sortBuilder.setLength(0);
        sortBuilder.append(" ORDER BY ").append(columnName);
        if (order == Order.ASC)
            sortBuilder.append(" ASC ");
        else
            sortBuilder.append(" DESC ");
        return this;
    }
}
