/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epitech.orm.query;

import epitech.orm.configuration.TableDefinition;
import epitech.orm.exception.IncompleteSQLRequestException;
import epitech.orm.query.builder.SQLInsertBuilder;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jalal
 */
public class InsertQuery extends AbstractQuery
{
    private SQLInsertBuilder    _insertBuilder;

    public InsertQuery(Connection connection, TableDefinition definition)
    {
        super(connection, definition);
        this._insertBuilder = new SQLInsertBuilder(definition);
        this._insertBuilder.insert();
    }
    
    public <T> InsertQuery  forColumn(String columnName, T value)
    {
        this._insertBuilder.forColumn(columnName, value);
        return this;
    }
    
    @Override
    public <T> T execute() throws SQLException
    {
        Statement   statement = null;
        String      query;
        
        try 
        {
            query = this._insertBuilder.build();
            statement = this._connection.createStatement();
            statement.executeUpdate(query);
        } 
        catch (IncompleteSQLRequestException ex) 
        {
            Logger.getLogger(InsertQuery.class.getName()).log(Level.SEVERE, null, ex.getMessage());
        } 
        catch (SQLException ex) 
        {
            throw new SQLException(ex.getMessage());
        }
        finally
        {
            if (statement != null)
            {
                try 
                {
                    statement.close();
                } 
                catch (SQLException ex) 
                {
                    Logger.getLogger(InsertQuery.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return null;
    }
    
}
