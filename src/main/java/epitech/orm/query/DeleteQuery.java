/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epitech.orm.query;

import epitech.orm.configuration.TableDefinition;
import epitech.orm.query.builder.SQLDeleteBuilder;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jalal
 */
public class DeleteQuery extends AbstractQueryFilterable
{

    public DeleteQuery(Connection connection,TableDefinition definition)
    {
        super(connection, definition);
    }
    
    @Override
    public <T> T execute() throws SQLException
    {
        String              query = null;
        Statement           statement = null;
        SQLDeleteBuilder    deleteBuilder = new SQLDeleteBuilder(this._definition);
        
        try 
        {            
            deleteBuilder.delete();
            if (this._whereClause != null)
                deleteBuilder.restrict(this._whereClause.getRestrictions());
            query = deleteBuilder.build();
            statement = this._connection.createStatement();
            statement.executeUpdate(query);
        } 
        catch (SQLException ex)
        {
            throw new SQLException(ex.getMessage());
        }
        finally
        {
            if (statement != null)
            {
                try 
                {
                    statement.close();
                } 
                catch (SQLException ex) 
                {
                    Logger.getLogger(DeleteQuery.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        
        return null;
    }
}
