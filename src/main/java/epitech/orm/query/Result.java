/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epitech.orm.query;

import java.util.List;

/**
 *
 * @author jalal
 */
public class Result<T>
{
    private List<T> _results;
    
    
    public Result(List<T> results)
    {
        this._results = results;
    }
    
    public <T> T unique()
    {
        if (_results == null)
        {
            return null;
        }
        else if (_results.isEmpty())
        {
            return null;
        }
        return (T)_results.get(0);
    }
    
    public List<T>  list()
    {
        return this._results;
    }
}
