/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epitech.orm.query;

import epitech.orm.configuration.TableDefinition;
import epitech.orm.query.builder.SQLSaveBuilder;
import epitech.orm.sql.Column;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jalal
 */
public class UpdateQuery extends AbstractQueryFilterable
{
    private SQLSaveBuilder  _updateBuilder = null;
    private boolean         _updateInitialized = false;
    
    public UpdateQuery(Connection connection, TableDefinition definition) 
    {
        super(connection, definition);
        this._updateBuilder = new SQLSaveBuilder(definition);
    }

    public <T> UpdateQuery  set(String columnName, T value)
    {
        if (this._updateInitialized)
        {
            _updateBuilder.set(columnName, value);
        }
        else
        {
            this._updateBuilder.update(columnName, value);
            this._updateInitialized = true;
        }
        return this;
    }
   
    
    @Override
    public <T> T execute() throws SQLException
    {
        Statement   statement = null;
        String  query;
        
        try {
            if (this._whereClause != null)
                this._updateBuilder.restrict(this._whereClause.getRestrictions());
            query = this._updateBuilder.build();
            statement = this._connection.createStatement();
            statement.executeUpdate(query);
        } 
        catch (SQLException ex) 
        {
            throw new SQLException(ex.getMessage());
        }
        finally
        {
            if (statement != null)
            {
                try 
                {
                    statement.close();
                } 
                catch (SQLException ex) 
                {
                    Logger.getLogger(UpdateQuery.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return null;
    }
    
}
