/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epitech.orm.query;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jalal
 */
public class WhereClause 
{
   private List<Restriction>       _restrictionList = new ArrayList<>();

   public WhereClause(Restriction restriction)
   {
       this._restrictionList.add(restriction);
   }
   
   public List<Restriction> getRestrictions()
   {
       return this._restrictionList;
   }
   
   public WhereClause       and(Restriction restrict)
   {
        this._restrictionList.get(this._restrictionList.size() - 1).setRestrictionTag(Restriction.RestrictionTag.And);
        this._restrictionList.add(restrict);
        return this;
   }
    
    public WhereClause        or(Restriction restrict)
    {
        this._restrictionList.get(this._restrictionList.size() - 1).setRestrictionTag(Restriction.RestrictionTag.And);
        this._restrictionList.add(restrict);
        return this;
    }
}
