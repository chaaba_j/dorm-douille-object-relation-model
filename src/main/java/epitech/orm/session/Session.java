/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epitech.orm.session;

import epitech.orm.configuration.TableDefinition;
import epitech.orm.configuration.Configuration;
import epitech.orm.exception.DORMException;
import epitech.orm.exception.SQLConnectionException;
import epitech.orm.exception.SQLDriverNotFound;
import epitech.orm.exception.TableDefinitionNotFound;
import epitech.orm.query.DeleteQuery;
import epitech.orm.query.InsertQuery;
import epitech.orm.query.Restriction;
import epitech.orm.query.SelectQuery;
import epitech.orm.query.UpdateQuery;
import epitech.orm.sql.Column;
import epitech.orm.sql.SQLRelation;
import epitech.orm.utils.ObjectUtils;
import epitech.orm.utils.ReflectUtils;
import java.io.FileNotFoundException;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jalal
 */
public class Session 
{
    private Map<Class<?>, TableDefinition>  _tableDefinitions;
    private Connection                      _connection = null;
    private Configuration                   _configuration = null;
    
    public Session(String file) throws FileNotFoundException, ClassNotFoundException, SQLException, TableDefinitionNotFound
    {
        this._configuration = Configuration.createFromFile(file);
        this.createDriver();
    }
    
    public Session(Configuration configuration) throws SQLDriverNotFound, SQLConnectionException
    {
        this._configuration = configuration;
        this.createDriver();
    }
    
    public void close()
    {
        if (this._connection != null)
        {
            try 
            {
                this._connection.close();
            }  
            catch (SQLException ex) 
            {
                Logger.getLogger(Session.class.getName()).log(Level.SEVERE, ex.getMessage());
            }
        }
    }
    
    private void createDriver() throws SQLConnectionException, SQLDriverNotFound
    {
        try
        {
            Class.forName(this._configuration.getDriver());
        
            this._connection = DriverManager.getConnection(this._configuration.getDatabaseUrl(),
                                                        this._configuration.getUsername(), this._configuration.getPassword());
        }
        catch (ClassNotFoundException ex)
        {
            throw new SQLDriverNotFound("Cannot found driver for classname : " + this._configuration.getDriver());
        }
        catch (SQLException ex)
        {
            throw new SQLConnectionException(ex.getMessage());
        }
    }
    
    public Transaction  beginTransaction() throws SQLException
    {
        return new Transaction(_connection);
    }
    
    private TableDefinition searchTable(Class theClass) throws TableDefinitionNotFound
    {
        List<TableDefinition> temp;
        
        temp = _configuration.getTableDefinition();
        
        for (TableDefinition definition : temp)
        {
            if (theClass.getName().compareTo(definition.getObjectClass().getName()) == 0)
                return definition;
        }
        throw new TableDefinitionNotFound("No such table for class : " + theClass.getName());
    }
    
    public SelectQuery select(Class theClass) throws TableDefinitionNotFound 
    {
        return new SelectQuery(this._connection, this.searchTable(theClass));
    }
    
    public <T> void delete(T obj) throws TableDefinitionNotFound, SQLException 
    {
        try 
        {
            TableDefinition definition = this.searchTable(obj.getClass());
            Column          column = definition.getPrimaryKey();
            DeleteQuery     query = new DeleteQuery(this._connection, definition);
            
            query.where(Restriction.equal(column.getColumnName(), ReflectUtils.getAttribute(obj, column.getObjectPropertyName())));
            query.execute();
        } 
        catch (NoSuchFieldException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException ex) 
        {
            throw new DORMException((ex.getMessage()));
        }
    }
    
    private <T> void    updateDependancyList(Entry<Column, T> parentObjInfo, List<Object> dependancies) throws TableDefinitionNotFound, SQLException
    {
        if (dependancies != null)
        {
            System.err.println("et marcha : " + dependancies.size());
            for (Object object : dependancies)
            {
                this.save(parentObjInfo, object);
            }
        }
    }
    
    private <T> void        updateDependancy(T obj, Column column, Column primaryKeyColumn) throws TableDefinitionNotFound, SQLException, NoSuchFieldException, IllegalAccessException, IllegalAccessException, InvocationTargetException, IllegalArgumentException, NoSuchMethodException
    {
        Entry<Column, T>  objInfo = new AbstractMap.SimpleEntry(column, ReflectUtils.getAttribute(obj, primaryKeyColumn.getObjectPropertyName()));
        Object            dependancy = null;
        
        try
        {
            dependancy = ReflectUtils.getAttribute(obj, column.getObjectPropertyName());
            if (column.getRelation().getRelationType() == SQLRelation.RelationType.OneToOne)
            {
                if (dependancy != null)
                {
                    this.save(objInfo, dependancy);
                }
            }
            else if (column.getRelation().getRelationType() == SQLRelation.RelationType.OneToMany)
            {
                this.updateDependancyList(objInfo, (List<Object>)dependancy);
            }
        }
        catch (NoSuchFieldException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException ex)
        {
            throw new DORMException(ex.getMessage());
        }
        
    }
    
    private Object          getParentKey(Object obj, Column column) throws NoSuchFieldException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException
    {
        Object              parent;
        
        parent = ReflectUtils.getAttribute(obj, column.getObjectPropertyName());
        if (parent != null)
            return ReflectUtils.getAttribute(parent, column.getRelation().getPropertyName());
        return null;
    }
    
    private <T, U> void    update(Entry<Column, U> parentObjInfo, T obj, TableDefinition definition) throws TableDefinitionNotFound, SQLException, NoSuchFieldException, IllegalAccessException, InvocationTargetException, IllegalArgumentException, NoSuchMethodException
    {
        UpdateQuery     query = new UpdateQuery(_connection, definition);
        Column          primaryKeyColumn = definition.getPrimaryKey();
        
        if (parentObjInfo != null)
            query.set(parentObjInfo.getKey().getColumnName(), parentObjInfo.getValue());
        for (Column column : definition.getColumns())
        {
            if (column.getRelation() != null)
            {
                if ( column.getRelation().getRelationType() == SQLRelation.RelationType.ManyToOne)
                {
                    query.set(column.getColumnName(), this.getParentKey(obj, column));
                }
                else
                {
                    this.updateDependancy(obj, column, primaryKeyColumn);
                }
            }
            else
            {
                try 
                {
                    query.set(column.getColumnName(), ReflectUtils.getAttribute(obj, column.getObjectPropertyName()));
                } 
                catch (NoSuchFieldException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException ex) 
                {
                    throw new DORMException(ex.getMessage());
                }
            }
        }
        try 
        {
            query.where(Restriction.equal(primaryKeyColumn.getColumnName(), ReflectUtils.getAttribute(obj, primaryKeyColumn.getObjectPropertyName())));
        } 
        catch (NoSuchFieldException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException ex) 
        {
            throw new DORMException(ex.getMessage());
        }
        query.execute();
    }
    
    private <T, U> T    insert(Entry<Column, U> parentObjInfo, T obj, TableDefinition definition) throws TableDefinitionNotFound, SQLException, NoSuchFieldException, IllegalAccessException, InvocationTargetException, IllegalArgumentException, NoSuchMethodException
    {
        T               insertedObject;
        List<Column>    dependancyColumns = new ArrayList();
        Object          value;
        Column          primaryKeyColumn = definition.getPrimaryKey();
        InsertQuery     query = new InsertQuery(_connection, definition);
        
        if (parentObjInfo != null)
            query.forColumn(parentObjInfo.getKey().getColumnName(), parentObjInfo.getValue());
        for (Column column : definition.getColumns())
        {
            if (column.getRelation() != null)
            {
                if (column.getRelation().getRelationType() == SQLRelation.RelationType.ManyToOne)
                {
                    query.forColumn(column.getColumnName(), this.getParentKey(obj, column));
                }
                else
                {
                    dependancyColumns.add(column);
                }
            }
            else if (!column.getMetaData().isAutoIncrement() 
                        && column.getSqlGeneratorFunction() == null)
            {
                try 
                {
                    value = ReflectUtils.getAttribute(obj, column.getObjectPropertyName());
                    if (value != null)
                    {
                        query.forColumn(column.getColumnName(), value);
                    }
                } 
                catch (NoSuchFieldException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException ex) 
                {
                    throw new DORMException(ex.getMessage());
                }
            }
        }
        query.execute();
        insertedObject =  (T) this.select(obj.getClass()).last().execute().unique();
        ObjectUtils.merge(insertedObject, obj);
        for (Column column : dependancyColumns)
        {
            this.updateDependancy(obj, column, primaryKeyColumn);
        }
        return obj;
    }

    private <T, U> T save(Entry<Column, U> parentInfo, T obj) throws SQLException, TableDefinitionNotFound
    {
        T               result;
        TableDefinition definition = this.searchTable(obj.getClass());
        Column          column = definition.getPrimaryKey();
        SelectQuery     query = new SelectQuery(_connection, definition);
        Column          primaryKeyColumn = definition.getPrimaryKey();
        
        try 
        {
            query.where(Restriction.equal(primaryKeyColumn.getColumnName(), ReflectUtils.getAttribute(obj, primaryKeyColumn.getObjectPropertyName())));
        
            result = (T) query.execute().unique();
            if (result == null)
                this.insert(parentInfo, obj, definition);
            else
                this.update(parentInfo, obj, definition);
        }
        catch (NoSuchFieldException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException ex)
        {
            throw new DORMException(ex.getMessage());
        }
        return obj;
    }
    
    public <T> T save(T obj) throws TableDefinitionNotFound, SQLException
    {
        return this.save(null, obj);
    }
}
