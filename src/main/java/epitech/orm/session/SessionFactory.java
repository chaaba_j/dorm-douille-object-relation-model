/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package epitech.orm.session;

import epitech.orm.configuration.Configuration;
import epitech.orm.exception.TableDefinitionNotFound;
import java.io.FileNotFoundException;
import java.sql.SQLException;

/**
 *
 * @author ledinh_a
 */
public class SessionFactory
{
    public static Session   create() throws FileNotFoundException, ClassNotFoundException, SQLException, TableDefinitionNotFound
    {
        return new Session(Configuration.createFromFile("conf.xml"));
    }
    
    public static Session   create(String filename) throws ClassNotFoundException, FileNotFoundException, SQLException, TableDefinitionNotFound
    {
        return new Session(Configuration.createFromFile(filename));
    }
}
