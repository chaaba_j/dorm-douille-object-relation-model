/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epitech.orm.session;

import java.sql.Connection;
import java.sql.SQLException;

/**
 *
 * @author jalal
 */
public class Transaction 
{
    private Connection  _connection;
    
    public Transaction(Connection connection) throws SQLException
    {
        this._connection = connection;
        this._connection.setAutoCommit(false);
    }
    
    public void commit() throws SQLException
    {
        this._connection.commit();
    }
    
    public void rollback() throws SQLException
    {
        this._connection.rollback();
    }
}
